'use strict';

const
  bodyParser = require(`body-parser`),
  express = require(`express`),

  app = express(),
  env = process.argv[2] || `prod`,
  port = env === `prod` ? 3000 : 2368;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(`${__dirname}/app/dist`));

app.post(`/api/create`, (req, res) => {
  const
    createName = req.body.name,
    createCap = req.body.cap,
    createType = req.body.type;

  console.log(`Game Created!`);
  console.log(`Name: ${createName}\nCap: ${createCap}\nPrivate: ${createType}`);
});

app.post(`/api/join`, (req, res) => {
  const joinName = req.body.name;

  console.log(`Joining Game: ${joinName}`);
});

app.get(`*`, (req, res) => {
  res.sendFile(`${__dirname}/app/dist/index.html`);
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
