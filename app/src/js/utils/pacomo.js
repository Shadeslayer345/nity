import {withPackageName} from 'react-pacomo';

export default const {
  decorator: pacomoDecorator,
  transformer: pacomoTransformer
} = withPackageName(`nity`);
