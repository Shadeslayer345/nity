/* eslint react/jsx-sort-props: 0 */
/* eslint react/jsx-max-props-per-line: 0 */
/* global Route */

import AltRouter from 'alt-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import React from 'react';
import {render} from 'react-dom';

import FormSelect from './components/app.js';

const routes = (
  <Route path={`/`} component={FormSelect} />
);

render(
  <AltRouter
      history={createBrowserHistory()}
      routes={routes}
  />, document.getElementById(`nity-root`));
