import {pacomoTransformer} from '../utils/pacomo.js';
import React, {PropTypes} from 'react';

const Card = ({value}) => {
  return (
    <div className={`nity-card`} >
      <h3>{value}</h3>
    </div>
  );
};

Card.displayName = `Card`;
Card.propTypes = {
  value: PropTypes.string.isRequired
};

export default pacomoTransformer(Card);
