import {pacomoTransformer} from '../utils/pacomo.js';
import React from 'react';

import CardList from './cardlist.jsx';
import Question from './question.jsx';

const App = () => {
  return (
    <div>
      <Question value={`I like to spend my Saturday nigths ____________`} />
      <CardList values={[`Card1`, `Card2`, `Card3`, `Card4`, `Card5`]} />
    </div>
  );
};

App.displayName = `App`;

export default pacomoTransformer(App);
