import {pacomoTransformer} from '../utils/pacomo.js';
import React, {PropTypes} from 'react';

import Form from './Form.js';

const FormSelect = ({createAction, createFields, createMethod, joinFields,
    joinAction, joinMethod, showCreate, showCreateForm, showJoin, showJoinForm,
    updateShow}) => {
  return (
    <div>
      <Form
          buttonValue={`Create Game`}
          fields={createFields}
          formAction={createAction}
          formMethod={createMethod}
          handleClick={updateShow.bind(this, `create`)}
          show={showCreate}
          showForm={showCreateForm}
      />
      <Form
          buttonValue={`Join Game`}
          fields={joinFields}
          formAction={joinAction}
          formMethod={joinMethod}
          handleClick={updateShow.bind(this, `join`)}
          show={showJoin}
          showForm={showJoinForm}
      />
    </div>
  );
};

FormSelect.displayName = `FormSelect`;
FormSelect.propTypes = {
  createAction: PropTypes.string.isRequired,
  createFields: PropTypes.arrayOf.string.isRequired,
  createMethod: PropTypes.string.isRequired,
  joinAction: PropTypes.string.isRequired,
  joinFields: PropTypes.arrayOf.string.isRequired,
  joinMethod: PropTypes.string.arrayOf.string.isRequired,
  showCreate: PropTypes.bool.isRequired,
  showCreateForm: PropTypes.bool.isRequired,
  showJoin: PropTypes.boo.isRequired,
  showJoinForm: PropTypes.bool.isRequired,
  updateShow: PropTypes.func
};

export default pacomoTransformer(FormSelect);
