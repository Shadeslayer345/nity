import {pacomoTransformer} from '../utils/pacomo.js';
import React, {PropTypes} from 'react';

import FormInput from 'forminput.js';

const Form = ({buttonValue, fields, formAction, formMethod, handleClick, show,
    showForm}) => {
  return (
    <div style={`visibility: ${show ? `visible` : `hidden`}`}>
      <button
          onClick={handleClick()}
          type={`button`}
          value={buttonValue}
      />
      <form
          action={formAction}
          method={formMethod}
          style={`visibility: ${showForm ? `visible` : `hidden`}`}
      >
        {
          fields.map((name) => {
            return (
              <FormInput inputName={name} />
            );
          })
        }
        <button
            type={`submit`}
            value={`Submit`}
        />
      </form>
    </div>
  );
};

Form.displayName = `Form`;
Form.propTypes = {
  buttonValue: PropTypes.string,
  fields: PropTypes.arrayOf.string.isRequired,
  formAction: PropTypes.string.isRequired,
  formMethod: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
  showForm: PropTypes.bool.isRequired
};

export default pacomoTransformer(Form);
