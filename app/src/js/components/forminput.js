import {pacomoTransformer} from '../util/pacomo.js';
import React, {PropTypes} from 'react';

const FormInput = ({inputName}) => {
  return (
    <input
        name={inputName}
        placeholder={name}
        type={`text`}
    />
  );
};

FormInput.displayName = `FormInput`;
FormInput.propTypes = {
  inputName: PropTypes.string.isRequired
};

export default pacomoTransformer(FormInput);
