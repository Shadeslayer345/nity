import {pacomoTransformer} from '../utils/pacomo.js';
import React, {PropTypes} from 'react';

const Question = ({value}) => {
  return (
    <div>
      <h2>{value}</h2>
    </div>
  );
};

Question.displayName = `Question`;
Question.propTypes = {
  value: PropTypes.string.isRequired
};

export default Question;
