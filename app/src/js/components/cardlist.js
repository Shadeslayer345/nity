import {pacomoTransformer} from '../utils/pacomo.js';
import React, {PropTypes} from 'react';

import Card from './card.js';

const CardList = ({values}) => {
  return (
    <div className={`nity-CardList`} >
      {values.map((content) => {
        return <Card value={content} />;
      })}
    </div>
  );
};

CardList.displayName = `CardList`;
CardList.propTypes = {
  values: PropTypes.string.isRequired
};

export default pacomoTransformer(CardList);
